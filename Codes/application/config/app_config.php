<?php
$config = array();

$config["level_codes"] = array(
    array("id"=>1, "name"=>"Secondary"),
    array("id"=>2, "name"=>"Higher Secondary"),
    array("id"=>3, "name"=>"Diploma"),
    array("id"=>4, "name"=>"Bachelors / Honors"),
    array("id"=>5, "name"=>"Masters"),
    array("id"=>6, "name"=>"Phd (doctor of philosophy)")
);


//Academic Level Codes
$config["academic_level_codes"] = array(
    "1"=>"Secondary",
    "2"=>"Higher Secondary",
    "3"=>"Diploma",
    "4"=>"Bachelors / Honors",
    "5"=>"Masters",
    "6"=>"Phd (doctor of philosophy)"
);

//Allowed academic results
$config["academic_results"] = array(
    "First Division/Class",
    "Second Division/Class",
    "Third Division/Class"
);


//Single Value configuration
$config["jobseeker_profile_picture_path"] = "./uploads/jobseeker/profile_picture/";
$config["default_js_profile_picture"] = "assets/core/images/pp.png";
$config["default_user_icon"] = "assets/core/icons/common_user.png";
$config["default_js_f_profile_picture"] = "assets/core/images/pp2.png";
$config["default_banner_picture"] = "assets/core/images/default_image.png";
$config["default_company_logo"] = "assets/core/images/no_logo.png";

//Service Types for Employer Profile
$config["org_service_types"] = array(
    "1" => "Software",
    "2" => "Hardware",
    "3" => "ITES",
    "4" => "BPO",
    "5" => "E-Comm.",
    "6" => "ISP",
    "7" => "System Integrator",
    "8" => "ICT",
);

//Job posting department
$config["job_department"] = array(
    "1" => "Information Technology",
    "2" => "Marketing",
    "3" => "Human Resource",
    "4" => "Sales",
    "5" => "Accounting & Finance",
    "6" => "Customer Service Support"
);

//Job Posting Employment type
$config["employment_type"] = array(
    "1" => "Permanent",
    "2" => "Contractual"
);

//Job Posting job level/position
$config["job_levels"] = array(
    "1" => "Entry",
    "2" => "Assosiate",
    "3" => "Senior-mid",
    "4" => "Senior",
    "5" => "Executive"
);

//Job Sources, It will be used as organization_type
$config["job_sources"] = array(
    "1" => "Company",
    "2" => "Govt. Jobs",
    "3" => "Social Media",
    "4" => "Multinational",
    "5" => "Intl Agency/NGO"
);

//Languages for Jobseeker CV
$config["jobseeker_languages"] = array(
	"Bangla",
	"English",
    "Bahasa Melayu"
);

//Reading skills for Jobseeker CV
$config["jobseeker_reading"] = array(
    "0" => "Not Mentioned",
	"1" => "Good",
    "2" => "Very Good",
    "3" => "Excellent"
);

//Writing skills for Jobseeker CV
$config["jobseeker_writing"] = array(
	"1" => "Good",
    "2" => "Very Good",
    "3" => "Excellent"
);

//Speaking skills for Jobseeker CV
$config["jobseeker_speaking"] = array(
	"1" => "Good",
    "2" => "Very Good",
    "3" => "Excellent"
);

//Listening skills for Jobseeker CV
$config["jobseeker_listening"] = array(
	"1" => "Good",
    "2" => "Very Good",
    "3" => "Excellent"
);

//Modified by FHS
//Listening skills for Jobseeker CV
$config["common_skills"] = array(
    "0" => "Not Mentioned",
	"1" => "Good",
    "2" => "Very Good",
    "3" => "Excellent"
);

//Modified by Ershadul
//Personality Question for jobseeker
$config["personality_general_question"] = array(
    "1" => "Set A",
    "2" => "Set B",
    "3" => "Set C"
);
//Time per question for general question, in seconds
$config['general_question_time'] = 10;


$config['renew_button_active_day'] = 4;
$config['extend_button_active_day'] = 4;
$config['monthly_document_submit_start_date'] = strtotime(date('Y-m').'-25 00:00:01');
$config['monthly_document_submit_end_date'] =  strtotime('+1 month', strtotime(date('Y-m').'-05 23:59:59'));

$config['monthly_document_review_start_date'] = strtotime(date('Y-m').'-25 00:00:01');
$config['monthly_document_review_end_date'] =  strtotime('+1 month', strtotime(date('Y-m').'-05 23:59:59'));

//Leave following variable TRUE if you want to make contract mgt active, Otherwise array(id, id, id) to allow specific employee
$config['contract_emp'] = TRUE;

?>

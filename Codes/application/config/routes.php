<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Registration Login
//UI Functions
$route["jobseeker-signup"] = "user/jobseeker";
//API Functions
$route["jobseeker-register"] = "user/jobseeker_register";
$route["jobseeker-login"] = "user/jobseeker_login";
$route["jobseeker-signout"] = "user/jobseeker_signout";
$route['mail-verification/(:num)'] = 'user/verify_email/$1';
$route['password-reset/(:num)'] = 'password/reset/$1';

//Jobseeker CV Related
$route["jobseeker-personal-info"] = "jobseeker/get_personal_info";
$route["jobseeker-profile-update"] = "jobseeker/update_profile";


//Jobseeker Apply Related
$route["job-apply/(:any)"] = "job_apply/index/$1";

//Common Public
$route["country-list"] = "data/get_countries";
$route["city-list"] = "data/get_cities";
$route["member-association-list"] = "data/get_member_association";
$route["feedback-contact-us"] = "contact_us/save_feedback";

//Employer Section
$route["employer-signup"] = "user/employer";
$route["employer-register"] = "user/employer_register";
$route["employer-login"] = "user/employer_signin";
$route["employer-signout"] = "user/employer_signout";

//guest user
$route["guest-login"] = "user/guestUserStore";

//chat
$route["question"] = "chat/question/index";
$route['question-store'] = "chat/question/store";

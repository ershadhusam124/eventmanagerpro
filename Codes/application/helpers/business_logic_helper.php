<?php
/*
    This file is created to develop business logic related helper structural functions
    which may deal with controller/model level logic.
    This file will be cloned as it is to backend module
*/

/*
*   Name:               update_job_status()
*   Params:             $job_post_id = job_post_id, $status = Status of the job
*   Description:        This function will update status of a job & return true /false
*   Called By:          most job post related functions
*/
function update_job_status($job_post_id, $status){
    $CI = &get_instance();
    $CI->load->model("job_post_model");
    $data = array();
    $data["job_post_id"] = $job_post_id;
    $data["post_status"] = $status;
    return $CI->job_post_model->update($data);
}

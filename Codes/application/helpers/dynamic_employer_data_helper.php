<?php

function generate_unique_name($field_name){
    $field_name = str_replace(" ", "__", $field_name);
    $field_name = str_replace("?", "__what__", $field_name);
    $field_name = str_replace(":", "__colon__", $field_name);
    $field_name = str_replace(";", "__semicolon__", $field_name);
    $field_name = str_replace("(", "__firstbracestart__", $field_name);
    $field_name = str_replace(")", "__firstbraceclose__", $field_name);
    return $field_name;
}

function revert_label($field_name){
    $field_name = str_replace("__", " ", $field_name);
    $field_name = str_replace("__what__", "?", $field_name);
    $field_name = str_replace("__colon__", ":", $field_name);
    $field_name = str_replace("__semicolon__", ";", $field_name);
    $field_name = str_replace("__firstbracestart__", "(", $field_name);
    $field_name = str_replace("__firstbraceclose__", ")", $field_name);
    return $field_name;
}

function generate_actual_field($field_id, $field_type, $field_name, $preloaded_values='', $value=''){
    if($field_type == 1){
        return generate_input_field($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 2){
        return generate_textarea_field($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 3){
        return generate_checkbox($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 4){
        return generate_radio_button($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 5){
        return generate_dropdown($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 6){
        return generate_date_field($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 7){
        return generate_multiselect_dropdown($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 8){
        return generate_number_field($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 9){
        return generate_file_field($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 10){
        return generate_database_table_select($field_id, $field_name, $preloaded_values, $value);
    }else if($field_type == 11){
        return generate_database_table_multiselect($field_id, $field_name, $preloaded_values, $value);
    }
}

//Type 1
function generate_input_field($field_id, $field_name, $preloaded_values='', $value=''){
    $value = trim($value);
    $new_field_name = generate_unique_name($field_name);
    $input = "<input type='text' class='form-control' name='data[$field_id]' id='id_{$new_field_name}' value='$value' />";
    return $input;
}

//Type 2
function generate_textarea_field($field_id, $field_name, $preloaded_values='', $value=''){
    $value = trim($value);
    $new_field_name = generate_unique_name($field_name);
    $input = "<textarea class='form-control' name='data[$field_id]' id='id_{$new_field_name}' >$value</textarea>";
    return $input;
}

//Type 3
function generate_radio_button($field_id, $field_name, $preloaded_values='', $checked_value=''){
    $checked_value = trim($checked_value);
    $input = "";
    $new_field_name = generate_unique_name($field_name);
    foreach ($preloaded_values as $key => $value) {
        $checked = $checked_value == $value ? "checked " : " ";
        $input .= '<div class="form-check-inline custom_profile_inputs">';
            $input .= "<input type='radio' id='id_{$new_field_name}_{$key}' $checked name='data[$field_id]' value='$value' />";
            $input .= "<label class='form-check-label' for='id_{$new_field_name}_{$key}'>$value</label>  &nbsp;";
        $input .= '</div>';
    }
    return $input;
}

//Type 4
function generate_checkbox($field_id, $field_name, $preloaded_values='', $checked_value=''){
    $checked_value = trim($checked_value);
    if($checked_value != ""){
        $checked_value = explode("|");
    }else{
        $checked_value = array();
    }
    $new_field_name = generate_unique_name($field_name);
    $input = "";
    foreach ($preloaded_values as $key => $value) {
        $checked = in_array($value, $checked_value) ? "checked " : " ";
        $input .= '<div class="democheck">';
            $input .= "<input type='checkbox' id='id_{$new_field_name}_{$key}' $checked name='data[$field_id][]' value='$value' />";
            $input .= "<label for='id_{$new_field_name}_{$key}'>$value</label>  &nbsp;";
        $input .= '</div>';
    }
    return $input;
}

//Type 5
function generate_dropdown($field_id, $field_name, $preloaded_values='', $checked_value=''){
    $checked_value = trim($checked_value);
    $new_field_name = generate_unique_name($field_name);
    $input = "<select class='form-control' name='data[$field_id]' id='id_{$new_field_name}'>";
    $input .= "<option value=''>Select here</option>";
    foreach ($preloaded_values as $key => $value) {
        $selected_value = $value == $checked_value ? "selected " : " ";
        $input .= "<option $selected_value value='$value'>$value</option>";
    }
    $input .= "</select>";
    return $input;
}

//Type 6
function generate_date_field($field_id, $field_name, $preloaded_values='', $value=''){
    $value = trim($value);
    $new_field_name = generate_unique_name($field_name);
    $input = "<input type='text' class='form-control date_field' name='data[$field_id]' id='id_{$new_field_name}' value='$value' />";
    return $input;
}

//Type 7
function generate_multiselect_dropdown($field_id, $field_name, $preloaded_values='', $checked_value=''){
    $checked_value = trim($checked_value);
    $checked_value = $checked_value != "" ? explode(",", $checked_value) : array();
    $new_field_name = generate_unique_name($field_name);
    $input = "<select class='form-control multiselect' multiple='multiple' name='data[$field_id][]' id='id_{$new_field_name}'>";
    //$input .= "<option value=''>Select here</option>";
    foreach ($preloaded_values as $key => $value) {
        $selected_value = in_array($value, $checked_value) ? "selected " : " ";
        $input .= "<option $selected_value value='$value'>$value</option>";
    }
    $input .= "</select>";
    return $input;
}

//Type 8
function generate_number_field($field_id, $field_name, $preloaded_values='', $value=''){
    $value = trim($value);
    $new_field_name = generate_unique_name($field_name);
    $input = "<input type='number' class='form-control' name='data[$field_id]' id='id_{$new_field_name}' value='$value' />";
    return $input;
}


//Type 9
function generate_file_field($field_id, $field_name, $preloaded_values='', $value=''){
    $new_field_name = generate_unique_name($field_name);
    $input = "<input type='file' id='id_{$new_field_name}' name='file_$field_id' class='form-control' id='tl'>";
    return $input;
}

//Type 10
function generate_database_table_select($field_id, $field_name, $preloaded_values='', $checked_value=''){
    $checked_value = trim($checked_value);
    $new_field_name = generate_unique_name($field_name);
    $input = "<select class='form-control ' name='data[$field_id]' id='id_{$new_field_name}'>";
    $input .= "<option value=''>Select here</option>";
    foreach ($preloaded_values as $key => $value) {
        $selected_value = $value->id == $checked_value ? "selected " : " ";
        $input .= "<option $selected_value value='{$value->id}'>{$value->name}</option>";
    }
    $input .= "</select>";
    return $input;
}


//Type 11
function generate_database_table_multiselect($field_id, $field_name, $preloaded_values='', $checked_value=''){
    $checked_value = trim($checked_value);
    $checked_value = $checked_value != '' ? explode(",", $checked_value) : array();
    $new_field_name = generate_unique_name($field_name);
    $input = "<select class='form-control multiselect' name='data[$field_id][]' multiple='multiple' id='id_{$new_field_name}'>";
    //$input .= "<option value=''>Select here</option>";
    foreach ($preloaded_values as $key => $value) {
        $selected_value = in_array($value->name, $checked_value) ? "selected " : " ";
        $input .= "<option $selected_value value='{$value->name}'>{$value->name}</option>";
    }
    $input .= "</select>";
    return $input;
}


function get_db_table_value($table_name){

    $CI = &get_instance();
    $CI->load->model("generic_model");
    $value_list = $CI->generic_model->get_values($table_name);
    return $value_list;
}

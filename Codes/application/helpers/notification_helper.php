<?php

//To -1 means to user is TT

// Get a reference to the controller object
global $CIN;
global $notification_type_messages;
$GLOBALS['notification_type'] = 0;
$GLOBALS['notification_title'] = "";
$CIN = get_instance();
$CIN->load->model("notification_model");



$notification_type_messages = array(
    "1"=>"Please fill your profile",
    "2"=>"Please fill your profile",
    "3"=>"New job posted",
    "4"=>"Applied to job",
    "5"=>"Interview request sent",
    "6"=>"Interview reminder",
    "7"=>"Responded",
    "8"=>"Requested for advance assesment",
    "9"=>"Reminder for advance assesment",
    "10"=>"CV screened",
    "11"=>"CV screened",
    "12"=>"Interview feedback",
    "13"=>"Bank info & signed document is submitted",
    "14"=>"First month payment & necessary documents are submitted",
    "15"=>"Your monthly payment is deposited by Techtalents",
    "16"=>"Contract extend request sent",
    "17"=>"Contract request accepted/rejected"
);




function apply_notification($type, $from_type, $to_type, $from, $to, $inserted_id, $link="", $custom_title=""){
    $GLOBALS['notification_type'] = $type;
    $GLOBALS['notification_title'] = $custom_title;
    if(function_exists("notification_func_$type")){
        return call_user_func_array("notification_func_$type", array($from_type, $to_type, $from, $to, $inserted_id, $link));
    }
    return FALSE;
}

function notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link=""){
    $data = array();
    $data["from_user_type"] = $from_type;
    $data["to_user_type"] = $to_type;
    $data["notification_from"] = $from;
    $data["notification_to"] = $to;
    $data["notification_type"] = $GLOBALS['notification_type'];
    $data["notification_title"] = $GLOBALS['notification_title'];
    return $data;
}

//Funct_1 & Funct_2 is responsible for profile fill-up reminder
function notification_func_1($from_type, $to_type, $from, $to, $inserted_id, $link=""){
    global $notification_type_messages;
    global $CIN;
    $title = $notification_type_messages[1];
    $jobseeker = $CIN->notification_model->get_jobseeker_details($to);
    if(!empty($jobseeker)){
        $percent = $jobseeker->profile_percent == NULL ? "0%" : "$jobseeker->profile_percent %";
        $title = "Reminder: complete your profile 100%. Currently, you completed $percent";
    }
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["notification_title"] = $title;
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}

function notification_func_2($from_type, $to_type, $from, $to, $inserted_id, $link=""){
    global $notification_type_messages;
    global $CIN;
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link="");
    $data["notification_title"] = $notification_type_messages[1];
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}


// New job post related notification function
function notification_func_3($from_type, $to_type, $from, $to, $inserted_id, $link=""){
    global $notification_type_messages;
    global $CIN;
    $job_post_details = $CIN->notification_model->get_job_detail($inserted_id);
    $title = $notification_type_messages[3];
    $title = " on role ";
    $title .= !empty($job_post_details) ? $job_post_details->role_name . " and category " . $job_post_details->category_name : "";

    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["notification_title"] = $notification_type_messages[3] . $title;
    $data["target_page_link"] = $link;

    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}

//Job application notification
function notification_func_4($from_type, $to_type, $from, $to, $inserted_id, $link=""){
    global $notification_type_messages;
    global $CIN;
    $job_apply_details = $CIN->notification_model->get_job_application_details($inserted_id);
    $title = $notification_type_messages[4];
    if(!empty($job_apply_details)){
        if($to_type == 3){
            $title = "$job_apply_details->jobseeker_name is applied on a job of $job_apply_details->organization_name";
        }else{
            $title = "$job_apply_details->jobseeker_name is applied on your posted job";
        }
    }
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["notification_title"] = $title;
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}

//Interview Feedback Related
function notification_func_12($from_type, $to_type, $from, $to, $inserted_id, $link){
    global $notification_type_messages;
    global $CIN;
    $notification_details = $CIN->notification_model->get_interview_feedback_details($inserted_id);
    $title = "Interview feedback is given by ";
    $title .= !empty($notification_details) ? $notification_details->organization_name . " on a applied job " : "";
    if($to_type == 1){
        $title1 = $title . ". Please check your mail.";
    }else{
        $title1 = $title . "for candidate " . $notification_details->jobseeker_name;
    }
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["notification_title"] = $title1;
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}

//Bank info and document submitted by jobseeker for new contract
function notification_func_13($from_type, $to_type, $from, $to, $inserted_id, $link){
    global $notification_type_messages;
    global $CIN;
    $notification_details = $CIN->notification_model->get_contract_details_generic($inserted_id);
    $title = "Bank info & signed document submitted ";
    $title .= !empty($notification_details) ? "by " . $notification_details->first_name . " on a new contract" : "";
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["notification_title"] = $title;
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}

//Bank info and document submitted by jobseeker for new contract
function notification_func_14($from_type, $to_type, $from, $to, $inserted_id, $link){
    global $notification_type_messages;
    global $CIN;
    $notification_details = $CIN->notification_model->get_contract_details_generic($inserted_id);
    $title = "First month payment & necessary documents are submitted ";
    $title .= !empty($notification_details) ? "by ". $notification_details->organization_name . " on a new contract" : "";
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["notification_title"] = $title;
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}


//Jobseeker monthly payment given by techtalents
function notification_func_15($from_type, $to_type, $from, $to, $inserted_id, $link){
    global $notification_type_messages;
    global $CIN;
    $notification_details = $CIN->notification_model->get_contract_details_generic($inserted_id);
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}

//Contract extend requested by employer
function notification_func_16($from_type, $to_type, $from, $to, $inserted_id, $link){
    global $notification_type_messages;
    global $CIN;
    $notification_details = $CIN->notification_model->get_contract_details_generic($inserted_id);
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}

//Contract extend requested by employer
function notification_func_17($from_type, $to_type, $from, $to, $inserted_id, $link){
    global $notification_type_messages;
    global $CIN;
    $notification_details = $CIN->notification_model->get_contract_details_generic($inserted_id);
    $data = notification_general_array($from_type, $to_type, $from, $to, $inserted_id, $link);
    $data["target_page_link"] = $link;
    if($CIN->notification_model->save($data)){
        return TRUE;
    }else{
        return FALSE;
    }
}

<?php
/**
* Description: This function will find current session of user.
*              It checks if user session is not null. If null,
*              then redirect to homepage. Otherwise, it checks
*              user account is email verified or not. If not, then
*              redirect to email unverified page
* Params: $data = Associative array
* Used By/ Dependency: N/A
**/
function auth_client(){
    $CI = &get_instance();
    $CI->load->library("session");

    //Check Already Logged In
    $user_id = $CI->session->userdata("user_id");
    $user_type = $CI->session->userdata("user_type");
    if($user_id == NULL || $user_type == NULL || $user_type != 1){
        redirect(base_url());
        exit();
    }
}

function auth_vendor(){
    $CI = &get_instance();
    $CI->load->library("session");

    //Check Already Logged In
    $user_id = $CI->session->userdata("user_id");
    $user_type = $CI->session->userdata("user_type");
    if($user_id == NULL || $user_type == NULL || $user_type != 2){
        redirect(base_url());
        exit();
    }
}

function auth_admin(){
    $CI = &get_instance();
    $CI->load->library("session");

    //Check Already Logged In
    $user_id = $CI->session->userdata("user_id");
    $user_type = $CI->session->userdata("user_type");
    if($user_id == NULL || $user_type == NULL || $user_type != 0){
        redirect(base_url());
        exit();
    }
}


/**
* Description: This function will find current session of user.
*              It checks if user session is not null. If null,
*              throw a json error unauthorized. Otherwise, it checks
*              user account is email verified or not. If not, then
*              throw a json error data unauthorized due to email is unverified
* Params: N/A
* Used By/ Dependency: N/A
**/
function auth_jobseeker_json(){
    $CI = &get_instance();
    $CI->load->library("session");
    $user_id = $CI->session->userdata("user_id");
    $user_type = $CI->session->userdata("user_type");
    if($user_id == NULL || $user_type == NULL || $user_type != 1){
        $result = array("status"=>405, "reason"=>"Sorry! You are not authorized to get this data");
        dump_json($result);
        exit();
    }else{
        $CI->load->model("app_user_model");
        $data = $CI->app_user_model->select_by_id($user_id);
        if($data->is_email_verified != 1){
            $result = array("status"=>405, "reason"=>"Sorry! You are not authorized because, your email is unverified");
            dump_json($result);
            exit();
        }
    }
}

function forward_access(){
    $CI = &get_instance();
    $CI->load->library("session");
    $user_id = $CI->session->userdata("user_id");
    $user_type = $CI->session->userdata("user_type");
    $jobseeker_id = $CI->session->userdata("jobseeker_id");
    if($user_id != NULL && $jobseeker_id != NULL){
        redirect(base_url()."jobseeker");
    }
}


/**
* Description: This function will find current session of user.
*              It checks if user session is not null. If null,
*              throw a json error unauthorized. Otherwise, it checks
*              user account is email verified or not. If not, then
*              throw a json error data unauthorized due to email is unverified
* Params: N/A
* Used By/ Dependency: N/A
**/
function auth_employer_json(){
    $CI = &get_instance();
    $CI->load->library("session");
    $user_id = $CI->session->userdata("user_id");
    $user_type = $CI->session->userdata("user_type");
    $employer_id = $CI->session->userdata("employer_id");
    if($user_id == NULL || $user_type == NULL || $employer_id == NULL){
        $result = array("status"=>405, "reason"=>"Sorry! You are not authorized to get this data");
        dump_json($result);
        exit();
    }else{
        $CI->load->model("app_user_model");
        $data = $CI->app_user_model->select_by_id($user_id);
        if($data->is_email_verified != 1){
            $result = array("status"=>405, "reason"=>"Sorry! You are not authorized because, your email is unverified");
            dump_json($result);
            exit();
        }
    }
}


function employer_forward_access(){
    $CI = &get_instance();
    $CI->load->library("session");
    $user_id = $CI->session->userdata("user_id");
    $user_type = $CI->session->userdata("user_type");
    $employer_id = $CI->session->userdata("employer_id");
    if($user_id != NULL && $employer_id != NULL){
        redirect(base_url()."employer/profile");
    }
}

?>

var URI_UPLOAD_EMAIL_FILE = BASE_URL + "client/invite/upload_email";

$("#btn_upload_guest").click(function() {
    var loader = $(this).children("i");
	if(loader.is(":visible")){
		return false;
	}
	loader.show();
    var sendData = {};
    var formData = new FormData($("#frm_invitation")[0]);
    sendData.url = URI_UPLOAD_EMAIL_FILE;
    sendData.content = formData;
    formDataSend(sendData, function(callback){
        loader.hide();
        if (callback.data != null) {
            var result = jsonProcess(callback.data);
            if (result.status == 200) {
                alertify.success(result.message);
                location.reload();
            } else if (result.status == 402) {
                alertify.error(result.reason);
                placeFormErrorMessages(result.error_list);
            } else {
                alertify.error(result.reason);
            }
        }
    });
});

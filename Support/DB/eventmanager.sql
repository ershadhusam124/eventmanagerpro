-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2020 at 08:30 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_app_users`
--

CREATE TABLE `tbl_app_users` (
  `user_id` bigint(20) NOT NULL,
  `email` varchar(128) CHARACTER SET utf8 NOT NULL,
  `password` varchar(128) CHARACTER SET utf8 NOT NULL,
  `user_type` tinyint(2) DEFAULT '3' COMMENT '0=Admin ,1=Client, 2=Vendor',
  `User_status` int(11) NOT NULL DEFAULT '0' COMMENT '0=created,1= active,2=deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_app_users`
--

INSERT INTO `tbl_app_users` (`user_id`, `email`, `password`, `user_type`, `User_status`) VALUES
(1, 'admin@gmail.com', '123456', 1, 1),
(2, 'admin@event.com', '123456', 0, 1),
(17, 'h1@gail.com', '123456', 1, 1),
(21, 'admin2@gmail.com', '123456', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_client`
--

CREATE TABLE `tbl_client` (
  `client_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `first_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(128) CHARACTER SET utf8 NOT NULL,
  `phn_number` varchar(128) CHARACTER SET utf8 NOT NULL,
  `address` varchar(128) CHARACTER SET utf8 NOT NULL,
  `gender` tinyint(4) DEFAULT NULL COMMENT '1=Male,2=Female',
  `image` varchar(128) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_client`
--

INSERT INTO `tbl_client` (`client_id`, `user_id`, `first_name`, `last_name`, `phn_number`, `address`, `gender`, `image`) VALUES
(1, 1, 'Md. Asmaul', 'Hosen', '01989898989', 'Mirpur-11', 1, './uploads/client/15880654072.png'),
(3, 17, 'Aas', 'Asss', '1212121212', 'Dhaka', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_decoration`
--

CREATE TABLE `tbl_decoration` (
  `decoration_id` bigint(20) NOT NULL,
  `decoration_name` varchar(128) NOT NULL,
  `decoration_description` varchar(1024) NOT NULL,
  `decoration_img` varchar(1024) NOT NULL,
  `decoration_price` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_decoration`
--

INSERT INTO `tbl_decoration` (`decoration_id`, `decoration_name`, `decoration_description`, `decoration_img`, `decoration_price`) VALUES
(6, 'asfsad', 'fsd edfrewrfew', './uploads/decoration/15879715802.png', '121'),
(7, 'New', 'tes', './uploads/decoration/1588529528d.png', '20000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

CREATE TABLE `tbl_event` (
  `event_id` bigint(20) NOT NULL,
  `event_name` varchar(128) NOT NULL,
  `event_category` bigint(20) NOT NULL COMMENT '1 = coporate, 2 = private, 3 = social',
  `event_type` bigint(20) NOT NULL,
  `event_city` varchar(32) NOT NULL,
  `event_loc` bigint(20) NOT NULL,
  `event_budget` varchar(32) NOT NULL,
  `event_date` date NOT NULL,
  `client_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_event`
--

INSERT INTO `tbl_event` (`event_id`, `event_name`, `event_category`, `event_type`, `event_city`, `event_loc`, `event_budget`, `event_date`, `client_id`) VALUES
(2, 'test', 2, 2, 'dhaka', 1, '50000', '0000-00-00', 1),
(3, 'hello Testing purpose', 2, 1, 'Dhaka', 2, '21000', '2020-04-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event_category`
--

CREATE TABLE `tbl_event_category` (
  `event_category_id` bigint(20) NOT NULL,
  `event_category` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_event_category`
--

INSERT INTO `tbl_event_category` (`event_category_id`, `event_category`) VALUES
(1, 'Corporate'),
(2, 'Private'),
(3, 'Social');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event_type`
--

CREATE TABLE `tbl_event_type` (
  `event_type_id` bigint(20) NOT NULL,
  `event_category_id` bigint(20) NOT NULL,
  `event_type` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_event_type`
--

INSERT INTO `tbl_event_type` (`event_type_id`, `event_category_id`, `event_type`) VALUES
(1, 1, 'Weeding'),
(2, 2, 'Haldi'),
(3, 1, 'Reception'),
(4, 2, 'Birthday'),
(5, 3, 'Engagement');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `staff_id` bigint(20) NOT NULL,
  `full_name` varchar(64) NOT NULL,
  `staff_email` varchar(64) NOT NULL,
  `phn_num` varchar(64) NOT NULL,
  `staff_loc` varchar(128) NOT NULL,
  `gender` tinyint(1) NOT NULL COMMENT '1=Male, 2=Female, 3=Others',
  `expected_salary` varchar(64) NOT NULL,
  `image` varchar(128) NOT NULL,
  `staff_type` tinyint(1) NOT NULL COMMENT '1=fresher, 2=experience',
  `experience` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`staff_id`, `full_name`, `staff_email`, `phn_num`, `staff_loc`, `gender`, `expected_salary`, `image`, `staff_type`, `experience`) VALUES
(1, 'hello', 'abc@gmail.com', '01010101', '1', 0, '1000', '', 2, 'hello'),
(2, 'Husam', 'husam@gmail.com', '01010101010', 'Dhanmondi', 0, '1000', '', 2, 'hello'),
(3, 'Husam', 'husam2@gmail.com', '01010101010', 'Dhanmondi', 0, '1000', '', 2, 'hello'),
(4, 'Husam', 'abv@gmail.com', '0101010101', 'Dhanmondi', 0, '1122', '', 2, 'hello'),
(5, 'Md. Asmaul hosen', 'nissan19rocks@gmail.com', '01898282828', 'Dhanmondi', 0, '1000', '', 1, ''),
(9, 'Md. Asmaul ', 'd@gmail.com', '12345567888', '1', 1, '12346', '', 1, 'Hello i am Hello i am Hello i am Hello i am Hello i am Hello i am Hello i am Hello i am Hello i am '),
(10, 'sadsad', 's@gmail.com', '1212121', '3', 1, '1121', '', 2, 'Heloow '),
(11, 'Md. Asmaul hosen', 'hg@gmail.com', '1212121212', '4', 1, '1212', './uploads/staff/15880194292.png', 1, 'afasr  e er qr resr');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_loc`
--

CREATE TABLE `tbl_staff_loc` (
  `staff_loc_id` int(11) NOT NULL,
  `staff_loc` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_staff_loc`
--

INSERT INTO `tbl_staff_loc` (`staff_loc_id`, `staff_loc`) VALUES
(1, 'Mohammadpur'),
(2, 'Chawkbazar'),
(3, 'Dhanmondi'),
(4, 'Agargaon');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vendor`
--

CREATE TABLE `tbl_vendor` (
  `vendor_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `company_name` varchar(64) NOT NULL,
  `vendor_type` bigint(20) NOT NULL,
  `address` varchar(64) NOT NULL,
  `city` varchar(64) NOT NULL,
  `phn_num` varchar(64) NOT NULL,
  `sucessful_event` varchar(64) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_vendor`
--

INSERT INTO `tbl_vendor` (`vendor_id`, `user_id`, `company_name`, `vendor_type`, `address`, `city`, `phn_num`, `sucessful_event`, `image`) VALUES
(10, 21, 'New', 4, 'dhaka', 'lalbagh', '01681654444', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vendor_service`
--

CREATE TABLE `tbl_vendor_service` (
  `id` int(11) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `service_name` varchar(128) NOT NULL,
  `service_desc` varchar(1024) NOT NULL,
  `service_img` varchar(512) NOT NULL,
  `service_price` int(15) NOT NULL,
  `event_category` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_vendor_service`
--

INSERT INTO `tbl_vendor_service` (`id`, `vendor_id`, `service_name`, `service_desc`, `service_img`, `service_price`, `event_category`) VALUES
(10, 10, 'hello', 'test', './uploads/service/1588530552d.png', 2000, 3),
(11, 10, 'yes', 'test', './uploads/service/1588530586.jpeg', 2000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vendor_type`
--

CREATE TABLE `tbl_vendor_type` (
  `vendor_type_id` bigint(20) NOT NULL,
  `vendor_name` varchar(128) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_vendor_type`
--

INSERT INTO `tbl_vendor_type` (`vendor_type_id`, `vendor_name`) VALUES
(1, 'Photography'),
(2, 'DJ'),
(3, 'Lighting'),
(4, 'Catering'),
(5, 'Vehicle'),
(6, 'Electronics'),
(7, 'Venue'),
(8, 'Singer'),
(9, 'Performer'),
(10, 'Mascott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_app_users`
--
ALTER TABLE `tbl_app_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_client`
--
ALTER TABLE `tbl_client`
  ADD PRIMARY KEY (`client_id`),
  ADD KEY `FK_tbl_client_user_id_tbl_app_users` (`user_id`);

--
-- Indexes for table `tbl_decoration`
--
ALTER TABLE `tbl_decoration`
  ADD PRIMARY KEY (`decoration_id`);

--
-- Indexes for table `tbl_event`
--
ALTER TABLE `tbl_event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `FK_tbl_event_2` (`event_type`),
  ADD KEY `FK_tbl_event_1` (`event_category`),
  ADD KEY `FK3_tbl_event` (`client_id`);

--
-- Indexes for table `tbl_event_category`
--
ALTER TABLE `tbl_event_category`
  ADD PRIMARY KEY (`event_category_id`);

--
-- Indexes for table `tbl_event_type`
--
ALTER TABLE `tbl_event_type`
  ADD PRIMARY KEY (`event_type_id`),
  ADD KEY `FK_tbl_event_type_event_category_id_tbl_event_category` (`event_category_id`) USING BTREE;

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `tbl_staff_loc`
--
ALTER TABLE `tbl_staff_loc`
  ADD PRIMARY KEY (`staff_loc_id`);

--
-- Indexes for table `tbl_vendor`
--
ALTER TABLE `tbl_vendor`
  ADD PRIMARY KEY (`vendor_id`),
  ADD KEY `FK_tbl_vendor_user_id_tbl_app_users` (`user_id`);

--
-- Indexes for table `tbl_vendor_service`
--
ALTER TABLE `tbl_vendor_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_vendor_service_tbl_event_category` (`event_category`),
  ADD KEY `tbl_vendor_service_tbl_vendor` (`vendor_id`);

--
-- Indexes for table `tbl_vendor_type`
--
ALTER TABLE `tbl_vendor_type`
  ADD PRIMARY KEY (`vendor_type_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_app_users`
--
ALTER TABLE `tbl_app_users`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_client`
--
ALTER TABLE `tbl_client`
  MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_decoration`
--
ALTER TABLE `tbl_decoration`
  MODIFY `decoration_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_event`
--
ALTER TABLE `tbl_event`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_event_category`
--
ALTER TABLE `tbl_event_category`
  MODIFY `event_category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_event_type`
--
ALTER TABLE `tbl_event_type`
  MODIFY `event_type_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `staff_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_staff_loc`
--
ALTER TABLE `tbl_staff_loc`
  MODIFY `staff_loc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_vendor`
--
ALTER TABLE `tbl_vendor`
  MODIFY `vendor_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_vendor_service`
--
ALTER TABLE `tbl_vendor_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_vendor_type`
--
ALTER TABLE `tbl_vendor_type`
  MODIFY `vendor_type_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
